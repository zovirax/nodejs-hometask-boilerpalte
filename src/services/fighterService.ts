import FighterRepository from '../repositories/fighterRepository';
import Fighter from '../models/Fighter';
import BaseService from './baseService'
import {throwError} from "../helpers/throwHelper";
import {notFound} from "../helpers/HttpError";


class FighterService extends BaseService<Fighter> {
    constructor() {
        super(FighterRepository);
    }

    getAll() {
        const all = super.getAll();
        return all.length ? all : throwError(notFound('Fighters not found'));
    }

    search(search: object) {
        return FighterRepository.getOne(search) || throwError(notFound('Fighter not found'));
    }
}

export default new FighterService();
