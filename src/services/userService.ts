import UserRepository from '../repositories/userRepository';
import User from '../models/User';
import BaseService from './baseService'
import {throwError} from "../helpers/throwHelper";
import {notFound} from "../helpers/HttpError";


class UserService extends BaseService<User>{
    constructor() {
        super(UserRepository);
    }
    getAll() {
        const all = super.getAll();
        return all.length ? all : throwError(notFound('Users not found'));
    }

    search(search: object) {
        return UserRepository.getOne(search) || throwError(notFound('User not found'));
    }
}

export default new UserService();
