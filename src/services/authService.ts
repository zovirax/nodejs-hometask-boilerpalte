import UserService from './userService';

class AuthService {
    login(userData: object) {
        return UserService.search(userData);
    }
}

export default new AuthService();
