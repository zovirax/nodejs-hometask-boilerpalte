import BaseRepository from '../repositories/baseRepository';

export default class BaseService<T> {
    constructor(protected readonly repository: BaseRepository<T>) {
    }

    getById(id: string) {
        return this.search({id});
    }

    getAll() {
        return this.repository.getAll();
    }

    search(search: object) {
        return this.repository.getOne(search);
    }

    create(user: T) {
        return this.repository.create(user);
    }

    deleteById(id: string) {
        return this.repository.delete(id);
    }

    update(id: string, user: T) {
        this.repository.update(id, user);
        return this.getById(id);
    }
}
