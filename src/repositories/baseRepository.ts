import {v4} from 'uuid';
import dbAdapter from '../config/db';
import CollectionName from './CollectionName';

type Entity = {
    id: string;
    createdAt: Date;
    updatedAt?: Date;
};

abstract class BaseRepository<T> {
    protected dbContext: any;

    protected constructor(protected collectionName: CollectionName) {
        this.dbContext = dbAdapter.get(collectionName);
    }

    // TODO: refactor(carry out the function to a helper)
    generateId = (): string => v4();

    getAll(): (T & Entity)[] {
        return this.dbContext.value();
    }

    public getOne(search: object): T & Entity {
        return this.dbContext.find(search).value();
    }

    create(data: T): T & Entity {
        const id = this.generateId(), createdAt = new Date();
        const list = this.dbContext.push({...data, id, createdAt}).write();
        return list.find((it: Entity) => it.id === id);
    }

    update(id: string, dataToUpdate: T) {
        const updatedAt = new Date();
        return this.dbContext.find({id}).assign({...dataToUpdate, updatedAt}).write();
    }

    delete(id: string) {
        return this.dbContext.remove({id}).write();
    }
}

export default BaseRepository;

