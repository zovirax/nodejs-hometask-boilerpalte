import BaseRepository from './baseRepository'
import CollectionName from './CollectionName'
import Fighter from '../models/Fighter';

class FighterRepository extends BaseRepository<Fighter> {
    constructor() {
        super(CollectionName.Fighters);
    }
}

export default new FighterRepository();
