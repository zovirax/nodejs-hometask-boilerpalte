import BaseRepository from './baseRepository'
import CollectionName from './CollectionName'
import User from '../models/User';

class UserRepository extends BaseRepository<User> {
    constructor() {
        super(CollectionName.Users);
    }
}

export default new UserRepository();
