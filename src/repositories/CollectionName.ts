enum CollectionName {
    Users = "users",
    Fighters = "fighters",
    Fights = "fights",
}

export default CollectionName;
