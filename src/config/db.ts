import path from 'path';
import low from 'lowdb';
import FileSync from 'lowdb/adapters/FileSync';

const dbPath = `${path.resolve()}/database.json`;

const adapter = new FileSync(dbPath);
const dbAdapter = low(adapter);
const defaultDb = {users: [], fighters: [], fights: []};
dbAdapter.defaults(defaultDb).write();

export default dbAdapter;
