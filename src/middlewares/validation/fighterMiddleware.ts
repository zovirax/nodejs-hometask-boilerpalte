import express from 'express';
import Fighter from '../../models/Fighter';
import {isStringValid, isDefenseValid} from '../../helpers/validationHelper';
import {badRequest} from '../../helpers/HttpError';

export default function (req: express.Request & { fighter?: Fighter }, _: express.Response, next: express.NextFunction) {
    const fighter = extractFighterFromRequest(req);
    validateFighter({fighter, next});
    req.fighter = fighter;
    next();
}

function validateFighter({fighter, next}: { fighter: Fighter, next: express.NextFunction }) {
    if (!isStringValid(fighter.name)) {
        next(badRequest('Field `name` is not valid'));
    } else if (!isDefenseValid(fighter)) {
        next(badRequest('Field `defense` is not valid'));
    } else if (fighter.power <= 0) {
        next(badRequest('Field `power` is not valid'));
    } else if (+fighter.health !== 100) {
        next(badRequest('Field `health` is not valid'));
    }
}

function extractFighterFromRequest(req: express.Request) {
    const {id, name, health, power, defense} = req.body;
    return {id, name, health, power, defense};
}

