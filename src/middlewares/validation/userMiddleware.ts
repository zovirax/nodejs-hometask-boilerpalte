import express from 'express';
import User from '../../models/User';
import {isStringValid, isEmailValid, isPasswordValid, isPhoneNumberValid} from '../../helpers/validationHelper';
import {badRequest} from '../../helpers/HttpError';

export default function (req: express.Request & { user?: User }, _: express.Response, next: express.NextFunction) {
    const user = extractUserFromRequest(req);
    validateUser({user, next});
    req.user = user;
    next();
}

function validateUser({user, next}: { user: User, next: express.NextFunction }) {
    if (!isEmailValid(user)) {
        next(badRequest('Field `email` is not valid'));
    } else if (!isPasswordValid(user)) {
        next(badRequest('Field `password` is not valid'));
    } else if (!isPhoneNumberValid(user)) {
        next(badRequest('Field `phoneNumber` is not valid'));
    } else if (!isStringValid(user.firstName)) {
        next(badRequest('Field `firstName` is not valid'));
    } else if (!isStringValid(user.lastName)) {
        next(badRequest('Field `lastName` is not valid'));
    }
}

function extractUserFromRequest(req: express.Request) {
    const {email, password, phoneNumber, firstName, lastName} = req.body;
    return {email, password, phoneNumber, firstName, lastName};
}

