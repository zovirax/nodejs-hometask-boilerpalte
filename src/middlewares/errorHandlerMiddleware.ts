import express from 'express';

import HttpError, {isHttpError} from '../helpers/HttpError';

function errorHandler(err: Error | HttpError, _: express.Request, res: express.Response, next: express.NextFunction) {
    process.env.NODE_ENV != 'production' ? console.error(err) : null;

    let {message} = err;
    const status = isHttpError(err) ? err.status : 500;

    if (res.headersSent) {
        return next(err);
    } else if (process.env.NODE_ENV === 'production' && status === 500) {
        // Hide internal message detail for security issues
        message = 'Internal Server Error';
    }
    res.status(status).send({error: true, message});
};

export default errorHandler;
