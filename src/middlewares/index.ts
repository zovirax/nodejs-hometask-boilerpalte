import express from 'express';
import cors from 'cors';
import errorHandlerMiddleware from './errorHandlerMiddleware';

export function preRoutes (app: express.Application) {
    app.use(cors());
    app.use(express.json());
    app.use(express.urlencoded({ extended: true }));
}

export function postRoutes(app: express.Application) {
    app.use(errorHandlerMiddleware);
}
