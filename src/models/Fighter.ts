export default interface Fighter {
    name: string;
    health: number;
    power: 0;
    defense: number;
}
