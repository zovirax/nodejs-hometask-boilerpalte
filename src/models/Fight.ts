export enum LogType {
    fighter1Shot = 'fighter1Shot',
    fighter2Shot = 'fighter2Shot',
    fighter1Health = 'fighter1Health',
    fighter2Health = 'fighter2Health'
}

export default interface Fight {
    fighter1: string;
    fighter2: string;
    logs: { [idx in LogType]?: number }[];
}
