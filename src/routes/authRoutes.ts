import express from 'express';
import authService from '../services/authService';

const router = express.Router();

router.post('/login', (req, res: express.Response, _: express.NextFunction) => {
    const result = authService.login(req.body);
    res.send(result);
});

export default router;
