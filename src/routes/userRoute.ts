import express from 'express';
import UserService from '../services/userService';
import userValidation from '../middlewares/validation/userMiddleware';
import User from '../models/User';


const router = express.Router();

type UserRequest = express.Request & { user?: User };

router.get('/', (_: express.Request, res: express.Response) => {
    res.send(UserService.getAll());
});

router.get('/:id', (req: express.Request, res: express.Response) => {
    const user = UserService.getById(req.params.id);
    res.send(user);
});

router.post('/', userValidation, (req: UserRequest, res: express.Response) => {
    const user = UserService.create(req.user!);
    res.send(user);
});

router.delete('/:id', (req: express.Request, res: express.Response) => {
    const user = UserService.deleteById(req.params.id);
    res.send(user);
});

router.put('/:id', userValidation, (req: UserRequest, res: express.Response) => {
    const user = UserService.update(req.params.id, req.user!);
    res.send(user);
});


export default router;
