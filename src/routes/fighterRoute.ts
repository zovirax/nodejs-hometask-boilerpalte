import express from 'express';
import FighterService from '../services/fighterService';
import Fighter from '../models/Fighter';
import fighterMiddleware from '../middlewares/validation/fighterMiddleware';


const router = express.Router();

type FighterRequest = express.Request & { fighter?: Fighter };

router.get('/', (_req: express.Request, res: express.Response) => {
    const fighters = FighterService.getAll();
    res.send(fighters);
});

router.get('/:id', (req: express.Request, res: express.Response) => {
    const fighter = FighterService.getById(req.params.id);
    res.send(fighter);
},);

router.post('/', fighterMiddleware, (req: FighterRequest, res: express.Response) => {
    const fighter = FighterService.create(req.body);
    res.send(fighter);
});

router.delete('/:id', (req: express.Request, res: express.Response) => {
    const fighter = FighterService.deleteById(req.params.id);
    res.send(fighter);
});

router.put('/:id', fighterMiddleware, (req: FighterRequest, res: express.Response) => {
    const fighter = FighterService.update(req.params.id, req.body);
    res.send(fighter);
});

export default router;
