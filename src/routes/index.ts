import express from 'express';
import userRoute from './userRoute';
import fighterRoute from './fighterRoute'
import authRoute from './authRoutes';

export default (app: express.Application) => {
    app.use('/api/users', userRoute);
    app.use('/api/fighters', fighterRoute);
    app.use('/api/auth/', authRoute);
};
