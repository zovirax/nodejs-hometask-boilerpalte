export default class HttpError extends Error {
    status: number;

    constructor(message: string, status: number) {
        super(message);

        if (status < 400 || status > 599) {
            throw new Error(`Expected error HTTP status (400 - 599), got ${status}`);
        }

        this.name = 'HttpError';
        this.status = status;
    }
};

export function badRequest(message?: string): HttpError {
    return new HttpError(message || 'Bad Request', 400);
}

export function notFound(message?: string): HttpError {
    return new HttpError(message || 'Not Found', 404);
}

export function isHttpError(err: Error): err is HttpError {
    return err instanceof HttpError;
}
