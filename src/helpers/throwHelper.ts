export const throwIf = (res: boolean, err: Error) => {
    if (res) {
        throw err;
    }
}

export const throwIfNot = (res: boolean, err: Error) => {
    if (!res) {
        throw err;
    }
}

export const throwError = (err: Error): never => {
    throw err;
}
