const emailRegexp = /^[\w.+\-]+@gmail\.com$/;
const phoneNumberRegexp = /\+380([0-9]{9})$/;
const minPasswordLength = 3;

const isEmailValid = ({email}: { email: string }): boolean => emailRegexp.test(email);

const isPasswordValid = ({password}: { password: string }) => !!password && password.length >= minPasswordLength;

const isStringValid = (str: string) => !!(str && str.trim());

const isPhoneNumberValid = ({phoneNumber}: { phoneNumber: string }) => phoneNumberRegexp.test(phoneNumber);

const isDefenseValid = ({defense}: { defense: number }) => defense >= 1 && defense <= 10;

export {
    isEmailValid,
    isPasswordValid,
    isStringValid,
    isPhoneNumberValid,
    isDefenseValid,
};
