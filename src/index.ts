import express from 'express';
import routes from './routes/index';
import {preRoutes, postRoutes} from './middlewares';

const app = express();

preRoutes(app);
routes(app);
app.get('/', express.static('./client/build'));
postRoutes(app);

const port = 3050;
app.listen(port, () => {
    console.log('Server has started')
});

export default app;

